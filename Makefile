TARGET=review_paper

# make pdf by default
all: ${TARGET}.pdf

${TARGET}.pdf: ${TARGET}.dvi
	@dvipdf  ${TARGET}.dvi
	rm -f ${TARGET}.{spl,lot,lof,log,aux,ps,dvi,bbl,blg,log,idx,toc}

${TARGET}.dvi : ${TARGET}.tex
	@latex ${TARGET}
	@bibtex ${TARGET}
	@latex ${TARGET}
	@latex ${TARGET}

clean:
	rm -f ${TARGET}.{spl,pdf,lot,lof,log,aux,ps,dvi,bbl,blg,log,idx,toc}

PHONY : all clean 

