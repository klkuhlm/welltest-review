import numpy as np
from glob import glob
import matplotlib.pyplot as plt
import subprocess

PIPE = subprocess.PIPE

fn = 'cape_cod_parameter_estimates.csv'

tab = np.loadtxt(fn,skiprows=3,delimiter=',',usecols=(1,3,4))

# columns used are: r, d (depth to top of screen), b (screen length)

wells = {}
fh = open(fn,'r')
lines = fh.readlines()[3:]
fh.close()

for j,line in enumerate(lines):
    wells[line.split(',',1)[0].strip('"')] = tab[j,:]

#Q = 42.8 # pumping rate (ft^3/min) for 72 hours
#Kr = 0.24 # ft^2/min
b = 160.0 # aquifer thickness (ft)
Sy = 0.23
Ss = 2.9E-5 # 1/ft
#kappa = 0.4
#beta = 1.0 # ft
#L = 19.8 # ft
#d = 13.2 # depth to top of pumping well screen (ft)
#l = 60.2 # depth to bottom of pumping well screen (ft)

tlog0 = -1
tlog1 = 4

# input template
inputfile = """1  %i  F  T  F    :: quiet?, model, dimensionless?, timeseries?, piezometer?
4.28D+1                 :: Q (volumetric pumping rate) [L^3/T]
6.02D+1   1.32D+1        :: l,d (depth to bottom/top of screened interval)
3.333D-1  3.333D-1     :: rw, rc (radius of well casing and tubing)
1.0D+0                  :: gamma (dimensionless wellbore skin)
1 0.0D+0 1.0D+0     :: pumping well time behavior and parameters
1.60D+2                 :: b (initial saturated thickness)
2.4D-1   4.0D-1         :: Kr,kappa (radial K and ratio Kz/Kr)
%.5f  %.5f         :: Ss,Sy
%.5f  1 -999.                   :: beta (Malama), Moench alphas
8.0D-1  7.0D-1   2.0D0  1.25D0   1.98D+1  2 20  :: M/N  a_c,a_k,  psi_a,psi_k, L, soln type,MN order
26  1.0D-8  1.0D-9      :: deHoog invlap;  M,alpha,tol
6  4                    :: tanh-sinh quad;  2^k-1 order, # extrapollation steps
1  1  10  50            :: G-L quad;  min/max zero split, # zeros to integrate, # abcissa/zero
timedata.dat  9999.9      :: file to read time data from (and default t otherwise)
spacedata.dat  %.5f   :: file to read space data from (and default r otherwise)
%.3f  %.3f  %i  1.67D-1  1.0       :: top,bot obs well screen, quad order across screen, rwobs, shape factor
unconfined.out
"""

for f in glob('F*.dat'):
    well = f[:4]+'-'+f[4:7] # put dash in name and drop suffix
    td,sd = np.loadtxt(f,skiprows=1,delimiter=',',unpack=True)

    plt.figure(1)
    # plot data as dots
    plt.loglog(td,sd,'k.')

    if well not in wells:
        print 'skipping',well,' distances not available'
    else:

        print well

        # monitoring well parameters
        r,topdepth,screen = wells[well]
        ztop = b - topdepth
        zbot = b - (topdepth + screen)

        if screen < 8.0:
            order = 3
        elif screen < 35.0:
            order = 5
        else:
            order = 7

        # early Hantush, late Hantush, neuman74
        pars = [(1,Ss,1.0,0.0,r,ztop,zbot,order),
                (1,Ss+Sy/b,1.0,0.0,r,ztop,zbot,order),
                (5,Ss,Sy,0.0,r,ztop,zbot,order)]

        plots = ['r--','g:','k-']

        maxval = 0.0

        for par,plot in zip(pars,plots):
            print par[0]
            fout = open('unconfined.in','w')
            fout.write(inputfile % par)
            fout.close()

            p = subprocess.Popen('./unconfined unconfined.in',
                                 shell=True,stdout=PIPE,stderr=PIPE)
            stdout,stderr = p.communicate()

            if not stderr == '':
                print 'unconfined ERROR:',stderr
            else:
                # load in results
                t,s = np.loadtxt('unconfined.out',skiprows=22,unpack=True,usecols=(0,1))
                if s.max() > maxval:
                    maxval = s.max()

                plt.loglog(t,s,plot)


    plt.ylim([10.0**np.floor(np.log10(sd.min())),maxval*1.05])
    plt.xlim([5.0E-2,7.0E+3])

    plt.xlabel('time (min)')
    plt.ylabel('drawdown (ft)')
    plt.savefig('cape_cod_'+well+'.eps')
    plt.close(1)

